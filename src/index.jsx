import React from "react"
import ReactDOM from "react-dom"
import { Main } from "./components/Main"
import { OtherMain } from "./components/OtherMain"

ReactDOM.render(<Main />,document.getElementById('root'))
ReactDOM.render(<OtherMain />,document.getElementById('other_root'))
