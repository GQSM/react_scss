const path = require('path');

module.exports = {
    entry: ['./src/index.jsx'],
    output: {
        filename: 'src/bundle.js',
        path: path.resolve(__dirname, './'),
    },
    module: {
        rules: [
            { test: /.jsx$/, exclude: /node_modules/, use: { loader: 'babel-loader', options: { presets: ['@babel/preset-react', '@babel/preset-env'] } } },
            { test: /.js$/, exclude: /node_modules/, use: { loader: 'babel-loader', options: { presets: ['@babel/preset-env'] } } },
            { test: /\.scss$/, use: [{loader: "style-loader" },{loader: "css-loader" },{loader: "sass-loader" }]}
        ]
    },
    devServer: {
        port: 8080
    }
};